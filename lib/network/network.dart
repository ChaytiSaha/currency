import 'package:currency/constants/api/api.dart';
import 'package:currency/constants/api/constant.dart';

class NetworkCall {
  //weather
  Future<String> loadCurrencyByMeasurement(String name) async {
    String apiName = ApiConstant.GET_CURRENCY + '/$name/BDT' + '?apiKey=${ApiConstant.API_KEY}';
    Uri api = Uri.parse(apiName);
    var response = await client.get(api);
    return response.body;
  }
}